from abc import ABC, abstractclassmethod

class Person(ABC):
  @abstractclassmethod
  def getFullName(self):
    pass
  def addRequest(self):
    pass
  def checkRequest(self):
    pass
  def addUser(self):
    pass

class Employee(Person):
  def __init__(self, firstName, lastName, email, department):
    super().__init__()
    self._firstName = firstName
    self._lastName = lastName
    self._email = email
    self._department = department
  # Abstract Methods
  def getFullName(self):
    return f"{self._firstName} {self._lastName}"
  def addRequest(self):
    return "Request has been added"
  def checkRequest(self):
    print()
  def addUser(self):
    print("User has been added") 
  def login(self):
    return f"{self._email} has logged in"
  def logout(self):
    return f"{self._email} has logged out"

    
    

class TeamLead(Person):
  def __init__(self, firstName, lastName, email, department):
    super().__init__()
    self._firstName = firstName
    self._lastName = lastName
    self._email = email
    self._department = department
    self._members = []
  # Abstract Methods
  def getFullName(self):
    return f"{self._firstName} {self._lastName}"
  def addRequest(self):
    print("Request has been added") 
  def checkRequest(self):
    print()
  def addUser(self):
    print("User has been added") 
  def login(self):
    print(f"{self._email} has logged in") 
  def logout(self):
    print(f"{self._email} has logged out") 
  def addMember(self, employee):
    self._members.append(employee)
  def get_members(self):
    return (self._members) 

class Admin(Person):
  def __init__(self, firstName, lastName, email, department):
    super().__init__()
    self._firstName = firstName
    self._lastName = lastName
    self._email = email
    self._department = department
    # Abstract Methods
  def getFullName(self):
    return f"{self._firstName} {self._lastName}"
  def addRequest(self):
    print("Request has been added") 
  def checkRequest(self):
    print()
  def addUser(self):
    return "User has been added"
  def login(self):
    print(f"{self._email} has logged in") 
  def logout(self):
    print(f"{self._email} has logged out") 

class Request():
  def __init__(self, name, requester, dateRequested):
    super().__init__()
    self._name = name
    self._requester = requester
    self._dateRequested = dateRequested
    self._status = ''

  def updateRequest(self):
    print()
  def closeRequest(self):
    self._status = "closed"
    return f"Request {self._name} has been {self._status}"
  def cancelRequest(self):
    self._status = "cancelled"
    print(f"Request {self._name} has been cancelled") 
  def set_status(self, status):
	  self._status = status


  

employee1 = Employee("John", "Doe", "dj@gmail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "js@gmail.com", "Marketing")
employee3 = Employee("Robert", "White", "rw@gmail.com", "Sales")
employee4 = Employee("Brandon", "Black", "bb@gmail.com", "Sales")

admin1 = Admin("Analyn", "Abella", "aabella@mail.com", "Marketing")

teamLead1 = TeamLead("Dran", "Spiff", "ds@mail.com", "Sales")

req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Analyn Abella", "Full name should be Analyn Abella"
assert teamLead1.getFullName() == "Dran Spiff", "Full name should be Dran Spiff"

assert employee2.login() == "js@gmail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "js@gmail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)


for indiv_emp in teamLead1.get_members():
	print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"
req2.set_status("closed")
print(req2.closeRequest())
